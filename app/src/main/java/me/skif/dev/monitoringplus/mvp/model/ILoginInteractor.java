package me.skif.dev.monitoringplus.mvp.model;

import me.skif.dev.monitoringplus.mvp.model.response.ResponseLogin;
import me.skif.dev.monitoringplus.util.Errors;

public interface ILoginInteractor {

  void login(String userName,
      String passWord,
      IValidationErrorListener validationErrorListener,
      IOnLoginFinishedListener loginFinishedListener);

  interface IOnLoginFinishedListener {

    void getUserData(ResponseLogin user);

    void errorMessage(String errorMessage);
  }

  interface IValidationErrorListener {

    void emailError(Errors code);

    void passwordError(Errors code);
  }
}
