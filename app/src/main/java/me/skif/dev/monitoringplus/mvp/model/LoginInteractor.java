package me.skif.dev.monitoringplus.mvp.model;

import android.text.TextUtils;
import me.skif.dev.monitoringplus.mvp.model.request.Login;
import me.skif.dev.monitoringplus.mvp.model.response.ResponseLogin;
import me.skif.dev.monitoringplus.service.APIError;
import me.skif.dev.monitoringplus.service.ErrorUtils;
import me.skif.dev.monitoringplus.service.ServiceWrapper;
import me.skif.dev.monitoringplus.util.Errors;
import me.skif.dev.monitoringplus.util.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInteractor implements ILoginInteractor {
  private ServiceWrapper serviceInstance;


  // region ===================== ILoginInteractor =====================
  @Override
  public void login(String userName,
      String passWord,
      IValidationErrorListener validationErrorListener,
      final IOnLoginFinishedListener loginFinishedListener) {
    if (isDataValid(userName, passWord, validationErrorListener)) {

      serviceInstance = new ServiceWrapper(null);

      Call<ResponseLogin> responseLoginCallback = serviceInstance.login(new Login(userName, passWord));

      responseLoginCallback.enqueue(new Callback<ResponseLogin>() {
        @Override
        public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {

          if (response.body() != null && response.isSuccessful()) {
            loginFinishedListener.getUserData(response.body());
          } else {

            if(response.errorBody() != null) {
              APIError error = ErrorUtils.parseError(response);
              loginFinishedListener.errorMessage(error.getMessage());
            } else {
              loginFinishedListener.errorMessage("Проблема в получении пользователя !! Попробуйте позже.");
            }
          }
        }

        @Override
        public void onFailure(Call<ResponseLogin> call, Throwable t) {
          loginFinishedListener.errorMessage("Проблема в получении пользователя !! Попробуйте позже.");
        }
      });
    }

  }
  // endregion ILoginInteractor

  private boolean isDataValid(String userName, String password, IValidationErrorListener validationErrorListener) {

    if (TextUtils.isEmpty(userName)) {

      validationErrorListener.emailError(Errors.EMAIL_INVALID);
      return false;

    } else if(!Utils.isValidEmail(userName)){

      validationErrorListener.emailError(Errors.EMAIL_INVALID);
      return false;

    } else if (TextUtils.isEmpty(password)) {

      validationErrorListener.passwordError(Errors.ENTER_PASSWORD);
      return false;

    } else if (password.length() < 6) {

      validationErrorListener.passwordError(Errors.PASSWORD_INVALID);
      return false;

    } else {
      return true;
    }
  }

}
