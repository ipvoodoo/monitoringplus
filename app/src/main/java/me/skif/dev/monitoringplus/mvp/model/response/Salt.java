package me.skif.dev.monitoringplus.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import me.skif.dev.monitoringplus.mvp.model.Data;

public class Salt {
  @SerializedName("errors")
  @Expose
  private List<Object> errors = null;
  @SerializedName("data")
  @Expose
  private Data data;

  public List<Object> getErrors() {
    return errors;
  }

  public void setErrors(List<Object> errors) {
    this.errors = errors;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }
}
