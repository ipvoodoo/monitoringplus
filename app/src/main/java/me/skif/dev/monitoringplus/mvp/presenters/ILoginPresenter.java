package me.skif.dev.monitoringplus.mvp.presenters;

public interface ILoginPresenter {
  void callLogin(String userName, String password);
}
