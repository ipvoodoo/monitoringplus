package me.skif.dev.monitoringplus.mvp.presenters;

import me.skif.dev.monitoringplus.mvp.model.ILoginInteractor;
import me.skif.dev.monitoringplus.mvp.model.response.ResponseLogin;
import me.skif.dev.monitoringplus.mvp.views.ILoginView;
import me.skif.dev.monitoringplus.util.Errors;

public class LoginPresenter implements ILoginPresenter {

  ILoginView mILoginView;
  ILoginInteractor mILoginInteractor;

  public LoginPresenter(ILoginView iloginView, ILoginInteractor ILoginInteractor) {
    mILoginView = iloginView;
    mILoginInteractor = ILoginInteractor;
  }

  // region ===================== ILoginPresenter =====================
  @Override
  public void callLogin(String userName, String password) {
    mILoginView.showLoading();
    mILoginInteractor.login(userName, password, new ILoginInteractor.IValidationErrorListener() {
      @Override
      public void emailError(Errors code) {
        mILoginView.hideLoading();
        mILoginView.setEmailError(code);
      }

      @Override
      public void passwordError(Errors code) {
        mILoginView.hideLoading();
        mILoginView.setPasswordError(code);
      }

    }, new ILoginInteractor.IOnLoginFinishedListener() {
      @Override
      public void getUserData(ResponseLogin user) {
        mILoginView.hideLoading();
        if (user != null) {
          mILoginView.loginSuccess(user);
        } else {
          mILoginView.loginFailure(Errors.LOGIN_FAILED);
        }
      }

      @Override
      public void errorMessage(String errorMessage) {
        mILoginView.hideLoading();
        mILoginView.loginFailure(errorMessage);
      }
    });
  }
  // endregion ILoginPresenter

}
