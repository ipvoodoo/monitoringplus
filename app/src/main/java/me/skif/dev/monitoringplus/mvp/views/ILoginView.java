package me.skif.dev.monitoringplus.mvp.views;

import me.skif.dev.monitoringplus.mvp.model.response.ResponseLogin;
import me.skif.dev.monitoringplus.util.Errors;

public interface ILoginView {

  void showLoading();

  void hideLoading();

  void setEmailError(Errors code);

  void setPasswordError(Errors code);

  void loginSuccess(ResponseLogin user);

  void loginFailure(Errors code);

  void loginFailure(String errMsg);
}
