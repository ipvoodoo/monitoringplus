package me.skif.dev.monitoringplus.service;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {
  public static APIError parseError(Response<?> response) {
    ServiceWrapper serviceWrapper = new ServiceWrapper(null);
    Converter<ResponseBody, APIError> converter =
        serviceWrapper.getRetrofit()
            .responseBodyConverter(APIError.class, new Annotation[0]);

    APIError error = new APIError();
    error.setStatus_code(010);

    try {
      error = converter.convert(response.errorBody());
    } catch (IOException e) {
      APIError networkError = new APIError();
      if (response.code() == 404) {
        networkError.setStatus_code(404);
        networkError.setMessage("Страница не найдена");
        return networkError;
      } else if (response.code() == 403) {
        networkError.setStatus_code(403);
        networkError.setMessage("Проблема при извлечении данных с сервера. Пожалуйста, повторите попытку позже.");
        return networkError;
      } else if (e.getCause() instanceof SocketTimeoutException) {
        networkError.setStatus_code(000);
        networkError.setMessage("Проблмема с сетью! \\nПлохое подключение или потеря соединения.");
        return networkError;
      } else if (e.getCause() instanceof UnknownHostException || e.getCause() instanceof ConnectException) {
        networkError.setStatus_code(000);
        networkError.setMessage("Нет связи с интернетом.");
        return networkError;
      }
//      else if (response.code() == 200){
//        networkError.setStatus_code(200);
//        networkError.setMessage(" успешное получение соли!");
//        return networkError;
//      }

      else {
        networkError.setStatus_code(000);
        networkError.setMessage("Проблема при извлечении данных с сервера. Пожалуйста, повторите попытку позже.");
        return networkError;
      }
    }

    return error;
  }
}
