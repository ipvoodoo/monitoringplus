package me.skif.dev.monitoringplus.service;

import me.skif.dev.monitoringplus.mvp.model.request.Login;
import me.skif.dev.monitoringplus.mvp.model.response.Credentials;
import me.skif.dev.monitoringplus.mvp.model.response.MyToken;
import me.skif.dev.monitoringplus.mvp.model.response.ResponseLogin;
import me.skif.dev.monitoringplus.mvp.model.response.Salt;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ServiceInterface {
  static final String LOGIN = "callLogin";

  @POST(LOGIN)
  Call<ResponseLogin> login(@Body Login loginCall);

  @Headers("Content-Type: application/json")
  @POST("/sessions/salt")
  Call<Salt> getSalt(@Body Login login);

  @Headers("Content-Type: application/x-www-form-urlencoded")
  @POST("/sessions")
  Call<MyToken> getToken(@Body Credentials credentials);
}
