package me.skif.dev.monitoringplus.service;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.concurrent.TimeUnit;
import me.skif.dev.monitoringplus.BuildConfig;
import me.skif.dev.monitoringplus.mvp.model.request.Login;
import me.skif.dev.monitoringplus.mvp.model.response.Credentials;
import me.skif.dev.monitoringplus.mvp.model.response.MyToken;
import me.skif.dev.monitoringplus.mvp.model.response.ResponseLogin;
import me.skif.dev.monitoringplus.mvp.model.response.Salt;
import me.skif.dev.monitoringplus.mvp.presenters.LoginPresenter;
import me.skif.dev.monitoringplus.util.Constant;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceWrapper {

  LoginPresenter mLoginPresenter;

  private ServiceInterface service;
  private Retrofit retrofit;
  private Interceptor mHeaderInterceptor;
  private String access_token = "";

  public ServiceWrapper(Interceptor headerInterceptor) {
    mHeaderInterceptor = headerInterceptor;
    retrofit = getRetrofit();
    service = retrofit.create(ServiceInterface.class);
  }

  public Retrofit getRetrofit() {

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    OkHttpClient client;

    if (mHeaderInterceptor != null) {
      OkHttpClient.Builder builder = new OkHttpClient.Builder()
          .addInterceptor(mHeaderInterceptor)
          .connectTimeout(120, TimeUnit.SECONDS)
          .readTimeout(90, TimeUnit.SECONDS);
      if (BuildConfig.DEBUG) {
        builder.addInterceptor(interceptor);
      }
      client = builder.build();
    } else {
      OkHttpClient.Builder builder = new OkHttpClient.Builder()
          .connectTimeout(120, TimeUnit.SECONDS)
          .readTimeout(90, TimeUnit.SECONDS);
      if (BuildConfig.DEBUG) {
        builder.addInterceptor(interceptor);
      }
      client = builder.build();
    }

    Gson gson = new GsonBuilder()
        .create();

    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(Constant.URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client)
        .build();

    Login mLogin = new Login();
    mLogin.setEmail("demo@m-plus.kz");
    mLogin.setPassword("123456");

    try {
      //      Отправляем запрос на сервер
      Call call = service.getSalt(mLogin);
      Response<Salt> response = call.execute();

      if (response.code() == 200) {
        String salt = response.body().getData().getSalt();
        Log.d("MyLogs", "соль = " + salt);
        tokenCall(encrypt(salt));
      }

    } catch (IOException e) {
      e.printStackTrace();
    }

    return retrofit;
  }

  //
    public Call<ResponseLogin> login(Login callingLogin) {
      return service.login(callingLogin);
//      return service.getSalt(callingLogin);
    }

  //  шифруем пароль
  private String encrypt(String salt) {
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");

      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("sha-256(sha-256(");
      stringBuilder.append("123456");
      stringBuilder.append(")+");
      stringBuilder.append(salt);
      stringBuilder.append(")");

      md.reset();
      md.update(stringBuilder.toString().getBytes("UTF-8"));
      byte[] digest = md.digest();
      String str = String.format("%0" + (digest.length * 2) +
          "X", new BigInteger(1, digest));
      Log.d("MyLogs", "digest = " + str);
      return str;

    } catch (Exception e) {
      Log.d("MyLogs", "ошибка шифрования пароля");
      e.printStackTrace();
      return null;
    }

  }


  //  Запрос на получение токена
  public int tokenCall(String token) {
    if (token.isEmpty()) {
      return 0;
    }
    int code = 0;
    Credentials credentials = new Credentials();
    credentials.setGrantType("password");
    credentials.setUsername("demo@m-plus.zk");
    credentials.setPassword(token);

    try {
      Call<MyToken> call = service.getToken(credentials);
      Response<MyToken> res = call.execute();
      code = res.code();
      if (code == 200) {
        access_token = res.body().getAccessToken();
        Log.d("MyLogs", "токен = " + access_token);
      }
    } catch (Exception e) {
      Log.d("MyLogs", "ошибка получения токена");
    }
    return code;
  }


  public String getAccess_token() {
    return access_token;
  }

}
