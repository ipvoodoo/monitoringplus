package me.skif.dev.monitoringplus.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.skif.dev.monitoringplus.R;
import me.skif.dev.monitoringplus.util.Constant;

public class HomeActivity extends BaseActivity {

  @BindView(R.id.activity_home_tv_message)
  TextView mActivityHomeTvMessage;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mActivityHomeTvMessage.setText(getIntent().getStringExtra(Constant.PASS_TO_HOME_MESSAGE));
  }

  // region ===================== BaseActivity =====================
  @Override
  public int getLayout() {
    return R.layout.activity_home;
  }

  @Override
  public void init() {
    ButterKnife.bind(this);
  }
  // endregion BaseActivity

}
