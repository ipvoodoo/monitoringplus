package me.skif.dev.monitoringplus.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.skif.dev.monitoringplus.R;
import me.skif.dev.monitoringplus.mvp.model.LoginInteractor;
import me.skif.dev.monitoringplus.mvp.model.response.ResponseLogin;
import me.skif.dev.monitoringplus.mvp.presenters.LoginPresenter;
import me.skif.dev.monitoringplus.mvp.views.ILoginView;
import me.skif.dev.monitoringplus.util.Constant;
import me.skif.dev.monitoringplus.util.Errors;
import me.skif.dev.monitoringplus.util.Utils;

public class LoginActivity extends BaseActivity implements ILoginView {

  @BindView(R.id.user_icon)
  ImageView mUserIcon;
  @BindView(R.id.et_email)
  EditText mEtEmail;
  @BindView(R.id.til_username)
  TextInputLayout mTilUsername;
  @BindView(R.id.password_icon)
  ImageView mPasswordIcon;
  @BindView(R.id.et_password)
  EditText mEtPassword;
  @BindView(R.id.til_password)
  TextInputLayout mTilPassword;
  @BindView(R.id.showpass_icon)
  ImageView mShowpassIcon;
  @BindView(R.id.tv_forgot_password)
  TextView mTvForgotPassword;
  @BindView(R.id.tv_register)
  TextView mTvRegister;
  @BindView(R.id.cardView)
  CardView mCardView;
  @BindView(R.id.btn_login)
  Button mBtnLogin;
  @BindView(R.id.auth_wrapper)
  LinearLayout mAuthWrapper;

  private ProgressDialog mProgressDialog;
  private LoginPresenter mPresenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  // region ===================== BaseActivity =====================
  @Override
  public int getLayout() {
    return R.layout.activity_main;
  }

  @Override
  public void init() {
    mProgressDialog = new ProgressDialog(LoginActivity.this);
    ButterKnife.bind(this);
    mPresenter = new LoginPresenter(this, new LoginInteractor());
  }
  // endregion BaseActivity

  // region ===================== Events =====================
  @OnClick(R.id.btn_login)
  public void submit() {
    if (mEtEmail != null && mEtPassword != null) {
      if (Utils.isNetworkAvailable(this)) {
        mPresenter.callLogin(mEtEmail.getText().toString(), mEtPassword.getText().toString());
      } else {
        Utils.displayCommonAlertDialog(this, this.getResources().getString(R.string.connection_issue_message));
      }
    }
  }

  @OnClick(R.id.tv_register)
  public void register() {
    Toast.makeText(getApplicationContext(), "Регистрация", Toast.LENGTH_SHORT).show();
  }

  @OnClick(R.id.tv_forgot_password)
  public void forgotPassword() {
    Toast.makeText(getApplicationContext(), "Забыли пароль?", Toast.LENGTH_SHORT).show();
  }

  // endregion Events

  // region ===================== ILoginView =====================
  @Override
  public void showLoading() {
    mProgressDialog.setTitle(null);
    mProgressDialog.setMessage(getResources().getString(R.string.activity_login_loading_message));
    mProgressDialog.show();
  }

  @Override
  public void hideLoading() {
    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.dismiss();
    }
  }

  @Override
  public void setEmailError(Errors code) {
    if (mEtEmail != null) {
      if (code.getId() == Errors.ENTER_EMAIL.getId()) {
        mEtEmail.setError(getResources().getString(R.string.activity_login_enter_email));
      } else if (code.getId() == Errors.EMAIL_INVALID.getId()) {
        mEtEmail.setError(getResources().getString(R.string.activity_login_email_invalid));
      }
    }
  }

  @Override
  public void setPasswordError(Errors code) {
    if (mEtPassword != null) {
      if (code.getId() == Errors.ENTER_PASSWORD.getId()) {
        mEtPassword.setError(getResources().getString(R.string.activity_login_enter_password));
      } else if (code.getId() == Errors.PASSWORD_INVALID.getId()) {
        mEtPassword.setError(getResources().getString(R.string.activity_login_password_error));
      }
    }
  }

  @Override
  public void loginSuccess(ResponseLogin user) {
    Intent openHomeScreen = new Intent(this, HomeActivity.class);
    openHomeScreen.putExtra(Constant.PASS_TO_HOME_MESSAGE, "Хоум Активити");
    startActivity(openHomeScreen);
    finish();
  }

  @Override
  public void loginFailure(Errors code) {
    if (code.getId() == 4) {
      Toast.makeText(this, getResources().getString(R.string.activity_login_fail_message), Toast.LENGTH_LONG).show();
    }
  }

  @Override
  public void loginFailure(String errMsg) {
    Toast.makeText(this, errMsg, Toast.LENGTH_LONG).show();
  }
  // endregion ILoginView
}
