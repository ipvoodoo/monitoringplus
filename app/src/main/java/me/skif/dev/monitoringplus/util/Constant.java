package me.skif.dev.monitoringplus.util;

public class Constant {

  public static final String URL = "https://dev.skif.me";
//  public static final String URL = "https://dev.skif.me/main.html?token=77321e40-4ba3-4e1c-8206-6423a8307479#monitorings";

  public static final String PASS_TO_HOME_MESSAGE = "home_message";
}
