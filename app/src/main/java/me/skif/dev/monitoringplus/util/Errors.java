package me.skif.dev.monitoringplus.util;

public enum  Errors {
  ENTER_EMAIL(0),
  EMAIL_INVALID(1),
  ENTER_PASSWORD(2),
  PASSWORD_INVALID(3),
  LOGIN_FAILED(4);

  private final int id;

  Errors(int id) {
    this.id = id;
  }

  public int getId() {
    return this.id;
  }
}
