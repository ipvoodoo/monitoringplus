package me.skif.dev.monitoringplus.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import me.skif.dev.monitoringplus.R;

public class Utils {

//  Проверка электронной почты.
  public static boolean isValidEmail(CharSequence target) {
    if (target == null) {
      return false;
    } else {
      return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
  }

  //  Метод используется для проверки доступности сети.
  //  @param context
  //  @return isNetAvailable
  public static boolean isNetworkAvailable(Context context) {

    boolean isNetAvailable = false;
    if (context != null) {

      final ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

      if (mConnectivityManager != null) {

        boolean mobileNetwork = false;
        boolean wifiNetwork = false;

        boolean mobileNetworkConnected = false;
        boolean wifiNetworkConnected = false;

        final NetworkInfo mobileInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        final NetworkInfo wifiInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mobileInfo != null) {

          mobileNetwork = mobileInfo.isAvailable();
        }

        if (wifiInfo != null) {

          wifiNetwork = wifiInfo.isAvailable();
        }

        if (wifiNetwork || mobileNetwork) {

          if (mobileInfo != null) {

            mobileNetworkConnected = mobileInfo.isConnectedOrConnecting();
          }
          wifiNetworkConnected = wifiInfo.isConnectedOrConnecting();
        }
        isNetAvailable = (mobileNetworkConnected || wifiNetworkConnected);
      }
    }

    return isNetAvailable;
  }


  /**
   * Common AppCompat Alert Dialog to be used in the Application everywhere
   *
   * @param mContext, Context of where to display
   */
  public static void displayCommonAlertDialog(final Context mContext, final String alertMessage) {
    try {
      android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mContext, R.style.StyleAppCompatAlertDialog);
      builder.setTitle(mContext.getResources().getString(R.string.app_name));
      builder.setMessage(alertMessage);
      builder.setPositiveButton(mContext.getResources().getString(R.string.activity_login_alert_ok), null);
      builder.show();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
